# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](https://semver.org/).


## [Unreleased]



## [0.2.0] - 2019-09-27
### Added

	- Advanced settings; i.e. Settings/All Settings/Games/Medio Volumetrico
	- Obsidian's texture

### Removed

	- Mapgen v6 Aliases (unsupported mapgen)



## [0.1.0] - 2019-09-26
### Added

	- Craftguide, Creative, Player API, SFINV, SFINV Buttons

### Removed

	- Playermodel

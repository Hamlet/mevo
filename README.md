### Medio Volumétrico
![Medio Volumétrico's screenshot](screenshot.png)  
**_Volumetric environment._**

**Version:** 0.2.0  
**Source code's license:** [EUPL v1.2][1] or later  
**Textures' license:** [CC BY-SA v4.0 International][2] or later  
**Sounds' licenses:** [CC-BY v3.0][3] [CC0][4]  
**Unless stated otherwise. Check each subfolder.

Textures have been made from scratch by Hamlet.  
Sounds have been based on other people's work, check  
/mevo/mods/default/sounds/ATTRIBUTION.txt


### Installation

Unzip the archive, rename the folder to mevo and place it in  
../minetest/games/

GNU+Linux - If you use a system-wide installation place it in  
~/.minetest/games/

For further information or help see:  
https://wiki.minetest.net/Games#Installing_games


### Configuration

Minetest Engine settings tab:
Settings/All Settings/Games/Medio Volumetrico



[1]: https://eur-lex.europa.eu/legal-content/EN/TXT/?uri=CELEX:32017D0863  
[2]: https://creativecommons.org/licenses/by-sa/4.0/  
[3]: https://creativecommons.org/licenses/by/3.0/  
[4]: https://creativecommons.org/publicdomain/zero/1.0/  

--[[

	Copyright © 2018-2019 Hamlet <hamlatmesehub@riseup.net>

	Licensed under the EUPL, Version 1.2 or – as soon they will be
	approved by the European Commission – subsequent versions of the
	EUPL (the "Licence");
	You may not use this work except in compliance with the Licence.
	You may obtain a copy of the Licence at:

	https://joinup.ec.europa.eu/software/page/eupl
	https://eur-lex.europa.eu/legal-content/EN/TXT/?uri=CELEX:32017D0863

	Unless required by applicable law or agreed to in writing,
	software distributed under the Licence is distributed on an
	"AS IS" basis,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
	implied.
	See the Licence for the specific language governing permissions
	and limitations under the Licence.

--]]


-- Used for localization

local S = minetest.get_translator("nodes_solid")

-- Used in "mapgen_stone"
minetest.register_node("default:01_01", {
	description = S("ŝtono"), -- Stone
	groups = {cracky = 1},
	drawtype = "normal",
	tiles = {"default_01_01.png"}
})

-- Used in "mapgen_dirt"
minetest.register_node("default:01_02", {
	description = S("tero"),
	groups = {falling_node = 1, crumbly = 2.5, soil = 1},
	drawtype = "normal",
	tiles = {"default_01_02.png"}
})

-- Used in "mapgen_dirt_with_grass"
minetest.register_node("default:01_03", {
	description = S("tero kun herbo"),
	groups = {falling_node = 1, crumbly = 2, soil = 1},
	drawtype = "normal",
	-- +Y, -Y, +X, -X, +Z, -Z
	tiles = {
		"default_01_03.png",
		"default_01_02.png",
		"default_01_03b.png",
		"default_01_03b.png",
		"default_01_03b.png",
		"default_01_03b.png",
	},
	drop = "default:01_02",
	sounds = {
		footstep = {name = "default_grass_footsteps", max_hear_distance = 5},
		--dig = <SimpleSoundSpec>, -- "__group" = group-based sound (default)
		--dug = <SimpleSoundSpec>,
		--place = <SimpleSoundSpec>,
		--place_failed = <SimpleSoundSpec>,
	}
})

-- Used in "mapgen_sand"
minetest.register_node("default:01_04", {
	description = S("sablo"),
	groups = {falling_node = 1, crumbly = 3},
	drawtype = "normal",
	tiles = {"default_01_04.png"},
	sounds = {
		footstep = {name = "default_sand_footsteps", max_hear_distance = 5},
		--dig = <SimpleSoundSpec>, -- "__group" = group-based sound (default)
		--dug = <SimpleSoundSpec>,
		--place = <SimpleSoundSpec>,
		--place_failed = <SimpleSoundSpec>,
	}
})

-- Used in "mapgen_gravel"
minetest.register_node("default:01_05", {
	description = S("gruzo"),
	groups = {falling_node = 1, crumbly = 2},
	drawtype = "normal",
	tiles = {"default_01_05.png"}
})

-- Used in "mapgen_desert_stone"
minetest.register_node("default:01_06", {
	description = S("ŝtono de la dezerto"),
	groups = {},
	drawtype = "normal",
	tiles = {"default_01_06.png"}
})

-- Used in "mapgen_desert_sand"
minetest.register_node("default:01_07", {
	description = S("sablo de la dezerto"),
	groups = {falling_node = 1, crumbly = 2.25},
	drawtype = "normal",
	tiles = {"default_01_07.png"},
	sounds = {
		footstep = {name = "default_sand_footsteps", max_hear_distance = 5},
		--dig = <SimpleSoundSpec>, -- "__group" = group-based sound (default)
		--dug = <SimpleSoundSpec>,
		--place = <SimpleSoundSpec>,
		--place_failed = <SimpleSoundSpec>,
	}
})

-- Used in "mapgen_dirt_with_snow"
minetest.register_node("default:01_08", {
	description = S("tero kun neĝo"),
	groups = {falling_node = 1, crumbly = 2, soil = 1},
	drawtype = "normal",
	-- +Y, -Y, +X, -X, +Z, -Z
	tiles = {
		"default_01_08.png",
		"default_01_02.png",
		"default_01_08b.png",
		"default_01_08b.png",
		"default_01_08b.png",
		"default_01_08b.png",
	},
	drop = "default:01_02"
})

-- Used in "mapgen_snowblock"
minetest.register_node("default:01_09", {
	description = S("bloko de neĝo"),
	groups = {falling_node = 1, crumbly = 3},
	drawtype = "normal",
	tiles = {"default_01_09.png"},
	sounds = {
		footstep = {name = "default_snow_footsteps", max_hear_distance = 5},
		--dig = <SimpleSoundSpec>, -- "__group" = group-based sound (default)
		--dug = <SimpleSoundSpec>,
		--place = <SimpleSoundSpec>,
		--place_failed = <SimpleSoundSpec>,
	}
})

-- Used in "mapgen_snow"
minetest.register_node("default:01_10", {
	description = S("neĝo"),
	groups = {falling_node = 1, crumbly = 3},
	drawtype = "nodebox",
	paramtype = "light",
	node_box = {
		type = "fixed",
		fixed = {
			{-0.5, -0.5, -0.5, 0.5, -0.25, 0.5},
		}
	},
	tiles = {"default_01_10.png"},
	buildable_to = true,
	sounds = {
		footstep = {name = "default_snow_footsteps", max_hear_distance = 5},
		--dig = <SimpleSoundSpec>, -- "__group" = group-based sound (default)
		--dug = <SimpleSoundSpec>,
		--place = <SimpleSoundSpec>,
		--place_failed = <SimpleSoundSpec>,
	}
})

-- Used in "mapgen_ice"
minetest.register_node("default:01_11", {
	description = S("glacio"),
	groups = {},
	drawtype = "normal",
	tiles = {"default_01_11.png"},
	sounds = {
		footstep = {name = "default_ice_footsteps", max_hear_distance = 5},
		--dig = <SimpleSoundSpec>, -- "__group" = group-based sound (default)
		--dug = <SimpleSoundSpec>,
		--place = <SimpleSoundSpec>,
		--place_failed = <SimpleSoundSpec>,
	}
})

-- Dirt with dry grass
minetest.register_node("default:01_12", {
	description = S("tero kun seka herbo"),
	groups = {falling_node = 1, crumbly = 2, soil = 1},
	drawtype = "normal",
	-- +Y, -Y, +X, -X, +Z, -Z
	tiles = {
		"default_01_12.png",
		"default_01_02.png",
		"default_01_12b.png",
		"default_01_12b.png",
		"default_01_12b.png",
		"default_01_12b.png",
	},
	drop = "default:01_02",
	sounds = {
		footstep = {name = "default_grass_footsteps", max_hear_distance = 5},
		--dig = <SimpleSoundSpec>, -- "__group" = group-based sound (default)
		--dug = <SimpleSoundSpec>,
		--place = <SimpleSoundSpec>,
		--place_failed = <SimpleSoundSpec>,
	}
})

-- Rainforest's dirt
minetest.register_node("default:01_13", {
	description = S("tero de la puvarbaro"),
	groups = {falling_node = 1, crumbly = 2, soil = 1},
	drawtype = "normal",
	-- +Y, -Y, +X, -X, +Z, -Z
	tiles = {
		"default_01_13.png",
		"default_01_02.png",
		"default_01_13b.png",
		"default_01_13b.png",
		"default_01_13b.png",
		"default_01_13b.png",
	},
	drop = "default:01_02",
	sounds = {
		footstep = {name = "default_grass_footsteps", max_hear_distance = 5},
		--dig = <SimpleSoundSpec>, -- "__group" = group-based sound (default)
		--dug = <SimpleSoundSpec>,
		--place = <SimpleSoundSpec>,
		--place_failed = <SimpleSoundSpec>,
	}
})

-- Scorched dirt with ash
minetest.register_node("default:01_14", {
	description = S("skorita tero kun cindro"),
	groups = {falling_node = 1, crumbly = 3},
	drawtype = "normal",
	-- +Y, -Y, +X, -X, +Z, -Z
	tiles = {
		"default_01_14.png",
		"default_01_14c.png",
		"default_01_14b.png",
		"default_01_14b.png",
		"default_01_14b.png",
		"default_01_14b.png",
	},
	drop = "default:01_15"
})

-- Scorched dirt
minetest.register_node("default:01_15", {
	description = S("skurita tero"),
	groups = {falling_node = 1, crumbly = 3},
	drawtype = "normal",
	-- +Y, -Y, +X, -X, +Z, -Z
	tiles = {"default_01_14c.png"},
	drop = "default:01_15"
})

-- Obsidian
minetest.register_node("default:01_16", {
	description = S("obsidiano"),
	groups = {cracky = 1},
	drawtype = "normal",
	-- +Y, -Y, +X, -X, +Z, -Z
	tiles = {"default_01_17.png"},
	drop = "default:01_16"
})

-- Hot stone
minetest.register_node("default:01_17", {
	description = S("varma ŝtono"),
	groups = {cracky = 1},
	drawtype = "normal",
	tiles = {
		{
			name = "default_01_15_anim.png",
			animation = {
				type = "vertical_frames",
				aspect_w = 16,
				aspect_h = 16,
				length = 3.0,
			},
		},
	},
	light_source = 7
})

-- Ash block
minetest.register_node("default:01_18", {
	description = S("bloko de cindro"),
	groups = {falling_node = 1, crumbly = 3},
	drawtype = "normal",
	-- +Y, -Y, +X, -X, +Z, -Z
	tiles = {"default_01_14.png"},
	drop = "default:01_18"
})

-- Hot desert stone
minetest.register_node("default:01_19", {
	description = S("varma ŝtono de la dezerto"),
	groups = {cracky = 1},
	drawtype = "normal",
	tiles = {
		{
			name = "default_01_16_anim.png",
			animation = {
				type = "vertical_frames",
				aspect_w = 16,
				aspect_h = 16,
				length = 3.0,
			},
		},
	},
	light_source = 7
})

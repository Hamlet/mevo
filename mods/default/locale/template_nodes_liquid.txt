# textdomain:nodes_liquid

# Water
akvo=

# Flowing water
akvo kiu fluas=

# River water
akvo de la rivero=

# Flowing river water
akvo de la rivero kiu fluas=

# Lava
lafo=

# Flowing lava
lafo kiu fluas=

# Freezing water
akvo kiu frostas=

# Flowing freezing water
akvo kiu frostas kiu fluas=

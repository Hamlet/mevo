--[[

	Copyright © 2018-2019 Hamlet <hamlatmesehub@riseup.net>

	Licensed under the EUPL, Version 1.2 or – as soon they will be
	approved by the European Commission – subsequent versions of the
	EUPL (the "Licence");
	You may not use this work except in compliance with the Licence.
	You may obtain a copy of the Licence at:

	https://joinup.ec.europa.eu/software/page/eupl
	https://eur-lex.europa.eu/legal-content/EN/TXT/?uri=CELEX:32017D0863

	Unless required by applicable law or agreed to in writing,
	software distributed under the Licence is distributed on an
	"AS IS" basis,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
	implied.
	See the Licence for the specific language governing permissions
	and limitations under the Licence.

--]]

-- Allow to toggle all ABMs on or off

if (minetest.settings:get_bool("default_abm_toggle") ~= false) then

	--
	-- Grass ABMs (from bare dirt to grass)
	--

	if (minetest.settings:get_bool("default_abm_grass_1") ~= false) then
		minetest.register_abm({
			label = "Dirt to dirt with grass",

			-- Nodes that can become dirt with grass.
			nodenames = {
				"default:01_02", -- Dirt
				"default:01_15"  -- Scorched dirt
			},

			-- Nodes required to be nearby.
			neighbors = {
				"default:01_03" -- Dirt with grass
			},

			interval = 45.0, -- Seconds
			chance = 6, -- 1: 100%; 2: 50%; 3: 33%; etc.
			catch_up = true, -- See lua_api.txt at line 4085 (v. 0.4.17.1)

			action = function(pos, node, active_object_count, active_object_count_wider)

				local pos_above = {
					x = pos.x,
					y = (pos.y + 1),
					z = pos.z
				}

				if (mevo.NodeAndLightCheck(pos_above, "air", 5) == true)
				then
					minetest.set_node(pos, {name = "default:01_03"})
				end
			end
		})
	end


	if (minetest.settings:get_bool("default_abm_grass_2") ~= false) then
		minetest.register_abm({
			label = "Dirt to dirt with dry grass",

			-- Nodes that can become dirt with dry grass.
			nodenames = {
				"default:01_02", -- Dirt
				"default:01_15"  -- Scorched dirt
			},

			-- Nodes required to be nearby.
			neighbors = {
				"default:01_12" -- Dirt with dry grass
			},

			interval = 45.0, -- Seconds
			chance = 6, -- 1: 100%; 2: 50%; 3: 33%; etc.
			catch_up = true, -- See lua_api.txt at line 4085 (v. 0.4.17.1)

			action = function(pos, node, active_object_count, active_object_count_wider)

				local pos_above = {
					x = pos.x,
					y = (pos.y + 1),
					z = pos.z
				}

				if (mevo.NodeAndLightCheck(pos_above, "air", 5) == true)
				then
					minetest.set_node(pos, {name = "default:01_12"})
				end
			end
		})
	end


	if (minetest.settings:get_bool("default_abm_grass_3") ~= false) then
		minetest.register_abm({
			label = "Dirt to dirt with rainforest grass",

			-- Nodes that can become dirt with rainforest grass.
			nodenames = {
				"default:01_02", -- Dirt
				"default:01_15"  -- Scorched dirt
			},

			-- Nodes required to be nearby.
			neighbors = {
				"default:01_13" -- Dirt with rainforest grass
			},

			interval = 45.0, -- Seconds
			chance = 6, -- 1: 100%; 2: 50%; 3: 33%; etc.
			catch_up = true, -- See lua_api.txt at line 4085 (v. 0.4.17.1)

			action = function(pos, node, active_object_count, active_object_count_wider)

				local pos_above = {
					x = pos.x,
					y = (pos.y + 1),
					z = pos.z
				}

				if (mevo.NodeAndLightCheck(pos_above, "air", 5) == true)
				then
					minetest.set_node(pos, {name = "default:01_13"})
				end
			end
		})
	end


	--
	-- Grass ABMs (from grass to bare dirt)
	--

	-- Whether if a grass covered node should be turned
	-- into a bare dirt node due to another node on it.
	if (minetest.settings:get_bool("default_abm_grass_4") ~= false) then
		minetest.register_abm({
			label = "Grass to dirt",

			-- Nodes that can be turned into bare dirt.
			nodenames = {
				"default:01_03", -- Dirt with grass
				"default:01_12", -- Dirt with dry grass
				"default:01_13" -- Dirt with rainforest grass
			},

			-- Nodes required to be nearby.
			neighbors = {},

			interval = 45.0, -- Seconds
			chance = 6, -- 1: 100%; 2: 50%; 3: 33%; etc.
			catch_up = true, -- See lua_api.txt at line 4085 (v. 0.4.17.1)

			action = function(pos, node, active_object_count, active_object_count_wider)

				local pos_above = {
					x = pos.x,
					y = (pos.y + 1),
					z = pos.z
				}

				local detected_node = minetest.get_node(pos_above).name

				if (detected_node ~= "air") then
					minetest.set_node(pos, {name = "default:01_02"})
				end
			end
		})
	end


	--
	-- Lava ABMs
	--

	if (minetest.settings:get_bool("default_abm_lava_1") ~= false) then
		minetest.register_abm({
			label = "Stone to hot stone",

			-- Nodes that can become hot stone.
			nodenames = {
				"default:01_01" -- Stone
			},

			-- Nodes required to be nearby.
			neighbors = {
				"default:02_05", -- Lava source
				"default:02_06"  -- Flowing lava
			},

			interval = 45.0, -- Seconds
			chance = 9, -- 1: 100%; 2: 50%; 3: 33%; etc.
			catch_up = true, -- See lua_api.txt at line 4085 (v. 0.4.17.1)

			action = function(pos, node, active_object_count, active_object_count_wider)
				minetest.set_node(pos, {name = "default:01_17"})
			end
		})
	end


	if (minetest.settings:get_bool("default_abm_lava_2") ~= false) then
		minetest.register_abm({
			label = "Hot stone to flowing lava",

			-- Nodes that can become hot stone.
			nodenames = {
				"default:01_17" -- Hot stone
			},

			-- Nodes required to be nearby.
			neighbors = {
				"default:02_05", -- Lava source
				"default:02_06"  -- Flowing lava
			},

			interval = 5.0, -- Seconds
			chance = 9, -- 1: 100%; 2: 50%; 3: 33%; etc.
			catch_up = true, -- See lua_api.txt at line 4085 (v. 0.4.17.1)

			action = function(pos, node, active_object_count, active_object_count_wider)
				minetest.add_node(pos, {name = "default:02_06", param1=238, param2=7})
			end
		})
	end


	if (minetest.settings:get_bool("default_abm_lava_3") ~= false) then
		minetest.register_abm({
			label = "Hot stone to stone",

			-- Nodes that can become stone.
			nodenames = {
				"default:01_17" -- Hot stone
			},

			-- Nodes required to be nearby.
			neighbors = {
				"air",
				"default:02_01", -- Water source
				"default:02_03", -- River water source
				"default:02_07", -- Freezing water source
				"default:01_11" -- Ice block
			},

			interval = 5.0, -- Seconds
			chance = 9, -- 1: 100%; 2: 50%; 3: 33%; etc.
			catch_up = true, -- See lua_api.txt at line 4085 (v. 0.4.17.1)

			action = function(pos, node, active_object_count, active_object_count_wider)
				minetest.add_node(pos, {name = "default:01_01"})
			end
		})
	end


	if (minetest.settings:get_bool("default_abm_lava_4") ~= false) then
		minetest.register_abm({
			label = "Desert stone to hot desert stone",

			-- Nodes that can become hot stone.
			nodenames = {
				"default:01_06" -- Desert stone
			},

			-- Nodes required to be nearby.
			neighbors = {
				"default:02_05", -- Lava source
				"default:02_06"  -- Flowing lava
			},

			interval = 45.0, -- Seconds
			chance = 9, -- 1: 100%; 2: 50%; 3: 33%; etc.
			catch_up = true, -- See lua_api.txt at line 4085 (v. 0.4.17.1)

			action = function(pos, node, active_object_count, active_object_count_wider)
				minetest.set_node(pos, {name = "default:01_19"})
			end
		})
	end


	if (minetest.settings:get_bool("default_abm_lava_5") ~= false) then
		minetest.register_abm({
			label = "Hot desert stone to flowing lava",

			-- Nodes that can become flowing lava.
			nodenames = {
				"default:01_19" -- Hot desert stone
			},

			-- Nodes required to be nearby.
			neighbors = {
				"default:02_05", -- Lava source
				"default:02_06"  -- Flowing lava
			},

			interval = 5.0, -- Seconds
			chance = 9, -- 1: 100%; 2: 50%; 3: 33%; etc.
			catch_up = true, -- See lua_api.txt at line 4085 (v. 0.4.17.1)

			action = function(pos, node, active_object_count, active_object_count_wider)
				minetest.add_node(pos, {name = "default:02_06", param1=238, param2=7})
			end
		})
	end


	if (minetest.settings:get_bool("default_abm_lava_6") ~= false) then
		minetest.register_abm({
			label = "Hot desert stone to desert stone",

			-- Nodes that can become desert stone.
			nodenames = {
				"default:01_19" -- Hot desert stone
			},

			-- Nodes required to be nearby.
			neighbors = {
				"air",
				"default:02_01", -- Water source
				"default:02_03", -- River water source
				"default:02_07", -- Freezing water source
				"default:01_11" -- Ice block
			},

			interval = 5.0, -- Seconds
			chance = 9, -- 1: 100%; 2: 50%; 3: 33%; etc.
			catch_up = true, -- See lua_api.txt at line 4085 (v. 0.4.17.1)

			action = function(pos, node, active_object_count, active_object_count_wider)
				minetest.add_node(pos, {name = "default:01_06"})
			end
		})
	end


	if (minetest.settings:get_bool("default_abm_lava_7") ~= false) then
		minetest.register_abm({
			label = "Dirt to scorched dirt",

			-- Nodes that can become scorched dirt.
			nodenames = {
				"default:01_02" -- Dirt
			},

			-- Nodes required to be nearby.
			neighbors = {
				"default:02_05", -- Lava source
				"default:02_06", -- Flowing lava
				"default:01_17", -- Hot stone
				"default:01_19" -- Hot desert stone
			},

			interval = 5.0, -- Seconds
			chance = 1, -- 1: 100%; 2: 50%; 3: 33%; etc.
			catch_up = true, -- See lua_api.txt at line 4085 (v. 0.4.17.1)

			action = function(pos, node, active_object_count, active_object_count_wider)
				minetest.set_node(pos, {name = "default:01_15"})
			end
		})
	end


	if (minetest.settings:get_bool("default_abm_lava_8") ~= false) then
		minetest.register_abm({
			label = "Dirt to scorched dirt with ash",

			-- Nodes that can become scorched dirt with ash.
			nodenames = {
				"default:01_03", -- Dirt with grass
				"default:01_08", -- Dirt with snow
				"default:01_12", -- Dirt with dry grass
				"default:01_13"  -- Rainforest's dirt
			},

			-- Nodes required to be nearby.
			neighbors = {
				"default:02_05", -- Lava source
				"default:02_06", -- Flowing lava
				"default:01_17", -- Hot stone
				"default:01_19" -- Hot desert stone
			},

			interval = 5.0, -- Seconds
			chance = 1, -- 1: 100%; 2: 50%; 3: 33%; etc.
			catch_up = true, -- See lua_api.txt at line 4085 (v. 0.4.17.1)

			action = function(pos, node, active_object_count, active_object_count_wider)
				minetest.set_node(pos, {name = "default:01_14"})
			end
		})
	end


	if (minetest.settings:get_bool("default_abm_lava_9") ~= false) then
		minetest.register_abm({
			label = "Dirt with ash to ash block",

			-- Nodes that can become ash blocks.
			nodenames = {
				"default:01_14", -- Dirt with ash.
				"default:01_15" -- Scorched dirt
			},

			-- Nodes required to be nearby.
			neighbors = {
				"default:02_05", -- Lava source
				"default:02_06", -- Flowing lava
				"default:01_17", -- Hot stone
				"default:01_19" -- Hot desert stone
			},

			interval = 5.0, -- Seconds
			chance = 9, -- 1: 100%; 2: 50%; 3: 33%; etc.
			catch_up = true, -- See lua_api.txt at line 4085 (v. 0.4.17.1)

			action = function(pos, node, active_object_count, active_object_count_wider)
				minetest.place_node(pos, {name = "default:01_18"})
			end
		})
	end


	if (minetest.settings:get_bool("default_abm_lava_10") ~= false) then
		minetest.register_abm({
			label = "Ash block to air",

			-- Nodes that can become air.
			nodenames = {
				"default:01_18" -- Ash block
			},

			-- Nodes required to be nearby.
			neighbors = {
				"default:02_05", -- Lava source
				"default:02_06", -- Flowing lava
				"default:01_17", -- Hot stone
				"default:01_19" -- Hot desert stone
			},

			interval = 5.0, -- Seconds
			chance = 9, -- 1: 100%; 2: 50%; 3: 33%; etc.
			catch_up = true, -- See lua_api.txt at line 4085 (v. 0.4.17.1)

			action = function(pos, node, active_object_count, active_object_count_wider)
				minetest.place_node(pos, {name = "air"})
			end
		})
	end


	if (minetest.settings:get_bool("default_abm_lava_11") ~= false) then
		minetest.register_abm({
			label = "Sands to obsidian",

			-- Nodes that can become obsidian.
			nodenames = {
				"default:01_04", -- Sand
				"default:01_07" -- Desert sand
			},

			-- Nodes required to be nearby.
			neighbors = {
				"default:02_05", -- Lava source
				"default:02_06", -- Flowing lava
				"default:01_17", -- Hot stone
				"default:01_19" -- Hot desert stone
			},

			interval = 5.0, -- Seconds
			chance = 1, -- 1: 100%; 2: 50%; 3: 33%; etc.
			catch_up = true, -- See lua_api.txt at line 4085 (v. 0.4.17.1)

			action = function(pos, node, active_object_count, active_object_count_wider)
				minetest.set_node(pos, {name = "default:01_16"})
			end
		})
	end


	if (minetest.settings:get_bool("default_abm_lava_12") ~= false) then
		minetest.register_abm({
			label = "Obsidian to scorched dirt",

			-- Nodes that can become obsidian.
			nodenames = {
				"default:01_16" -- Obsidian
			},

			-- Nodes required to be nearby.
			neighbors = {
				"default:02_05", -- Lava source
				"default:02_06", -- Flowing lava
				"default:01_17", -- Hot stone
				"default:01_19" -- Hot desert stone
			},

			interval = 5.0, -- Seconds
			chance = 9, -- 1: 100%; 2: 50%; 3: 33%; etc.
			catch_up = true, -- See lua_api.txt at line 4085 (v. 0.4.17.1)

			action = function(pos, node, active_object_count, active_object_count_wider)
				minetest.set_node(pos, {name = "default:01_15"})
			end
		})
	end


	if (minetest.settings:get_bool("default_abm_lava_13") ~= false) then
		minetest.register_abm({
			label = "Water to air #1",

			-- Nodes that can become air.
			nodenames = {
				"default:02_02", -- Flowing water
				"default:02_04", -- Flowing river water
				"default:02_08" -- Flowing freezing water
			},

			-- Nodes required to be nearby.
			neighbors = {
				"default:02_05" -- Lava source
			},

			interval = 5.0, -- Seconds
			chance = 1, -- 1: 100%; 2: 50%; 3: 33%; etc.
			catch_up = true, -- See lua_api.txt at line 4085 (v. 0.4.17.1)

			action = function(pos, node, active_object_count, active_object_count_wider)
				minetest.set_node(pos, {name = "air"})
				minetest.sound_play(
					"default_steam",
					{
						pos = pos,
						gain = 0.5,
						max_hear_distance = 10
					}
				)
			end
		})
	end


	if (minetest.settings:get_bool("default_abm_lava_14") ~= false) then
		minetest.register_abm({
			label = "Water to air #2",

			-- Nodes that can become air.
			nodenames = {
				"default:02_01", -- Water source
				"default:02_03", -- River water source
				"default:02_07" -- Freezing water source
			},

			-- Nodes required to be nearby.
			neighbors = {
				"default:02_05" -- Lava source
			},

			interval = 5.0, -- Seconds
			chance = 2, -- 1: 100%; 2: 50%; 3: 33%; etc.
			catch_up = true, -- See lua_api.txt at line 4085 (v. 0.4.17.1)

			action = function(pos, node, active_object_count, active_object_count_wider)
				minetest.set_node(pos, {name = "air"})
				minetest.sound_play(
					"default_steam",
					{
						pos = pos,
						gain = 0.5,
						max_hear_distance = 10
					}
				)
			end
		})
	end


	if (minetest.settings:get_bool("default_abm_lava_15") ~= false) then
		minetest.register_abm({
			label = "Snow to flowing water",

			-- Nodes that can become water.
			nodenames = {
				"default:01_10" -- Snow
			},

			-- Nodes required to be nearby.
			neighbors = {
				"default:02_05", -- Lava source
				"default:02_06", -- Flowing lava
				"default:01_17", -- Hot stone
				"default:01_19" -- Hot desert stone
			},

			interval = 5.0, -- Seconds
			chance = 1, -- 1: 100%; 2: 50%; 3: 33%; etc.
			catch_up = true, -- See lua_api.txt at line 4085 (v. 0.4.17.1)

			action = function(pos, node, active_object_count, active_object_count_wider)
				minetest.add_node(pos, {name = "default:02_02", param1=238, param2=4})
				minetest.sound_play(
					"default_steam",
					{
						pos = pos,
						gain = 0.5,
						max_hear_distance = 10
					}
				)
			end
		})
	end


	if (minetest.settings:get_bool("default_abm_lava_16") ~= false) then
		minetest.register_abm({
			label = "Snow block to flowing water",

			-- Nodes that can become water.
			nodenames = {
				"default:01_09" -- Snow
			},

			-- Nodes required to be nearby.
			neighbors = {
				"default:02_05", -- Lava source
				"default:02_06", -- Flowing lava
				"default:01_17", -- Hot stone
				"default:01_19" -- Hot desert stone
			},

			interval = 5.0, -- Seconds
			chance = 1, -- 1: 100%; 2: 50%; 3: 33%; etc.
			catch_up = true, -- See lua_api.txt at line 4085 (v. 0.4.17.1)

			action = function(pos, node, active_object_count, active_object_count_wider)
				minetest.add_node(pos, {name = "default:02_02", param1=238, param2=4})
				minetest.sound_play(
					"default_steam",
					{
						pos = pos,
						gain = 0.5,
						max_hear_distance = 10
					}
				)
			end
		})
	end


	if (minetest.settings:get_bool("default_abm_lava_17") ~= false) then
		minetest.register_abm({
			label = "Ice to flowing icy water",

			-- Nodes that can become water.
			nodenames = {
				"default:01_11" -- Ice
			},

			-- Nodes required to be nearby.
			neighbors = {
				"default:02_05", -- Lava source
				"default:02_06", -- Flowing lava
				"default:01_17", -- Hot stone
				"default:01_19" -- Hot desert stone
			},

			interval = 5.0, -- Seconds
			chance = 1, -- 1: 100%; 2: 50%; 3: 33%; etc.
			catch_up = true, -- See lua_api.txt at line 4085 (v. 0.4.17.1)

			action = function(pos, node, active_object_count, active_object_count_wider)
				minetest.add_node(pos, {name = "default:02_08", param1=238, param2=7})
				minetest.sound_play(
					"default_steam",
					{
						pos = pos,
						gain = 0.5,
						max_hear_distance = 10
					}
				)
			end
		})
	end


	--
	-- Water ABMs
	--

	if (minetest.settings:get_bool("default_abm_water_1") ~= false) then
		minetest.register_abm({
			label = "Lava to stone #1",

			-- Nodes that can become stone.
			nodenames = {
				"default:02_05" -- Lava source
			},

			-- Nodes required to be nearby.
			neighbors = {
				"default:02_01", -- Water source
				"default:02_03",  -- River water source
				"default:02_07" -- Freezing water source
			},

			interval = 5.0, -- Seconds
			chance = 2, -- 1: 100%; 2: 50%; 3: 33%; etc.
			catch_up = true, -- See lua_api.txt at line 4085 (v. 0.4.17.1)

			action = function(pos, node, active_object_count, active_object_count_wider)
				minetest.set_node(pos, {name = "default:01_01"})
			end
		})
	end


	if (minetest.settings:get_bool("default_abm_water_2") ~= false) then
		minetest.register_abm({
			label = "Lava to stone #2",

			-- Nodes that can become stone.
			nodenames = {
				"default:02_05" -- Lava source
			},

			-- Nodes required to be nearby.
			neighbors = {
				"default:02_02", -- Flowing water
				"default:02_04", -- Flowing river water
				"default:02_08" -- Flowing freezing water
			},

			interval = 5.0, -- Seconds
			chance = 10, -- 1: 100%; 2: 50%; 3: 33%; etc.
			catch_up = true, -- See lua_api.txt at line 4085 (v. 0.4.17.1)

			action = function(pos, node, active_object_count, active_object_count_wider)
				minetest.set_node(pos, {name = "default:01_01"})
			end
		})
	end


	if (minetest.settings:get_bool("default_abm_water_3") ~= false) then
		minetest.register_abm({
			label = "Lava to air",

			-- Nodes that can become air.
			nodenames = {
				"default:02_06"  -- Flowing lava
			},

			-- Nodes required to be nearby.
			neighbors = {
				"default:02_02", -- Flowing water
				"default:02_04", -- Flowing river water
				"default:02_08" -- Flowing freezing water
			},

			interval = 5.0, -- Seconds
			chance = 50, -- 1: 100%; 2: 50%; 3: 33%; etc.
			catch_up = true, -- See lua_api.txt at line 4085 (v. 0.4.17.1)

			action = function(pos, node, active_object_count, active_object_count_wider)
				minetest.set_node(pos, {name = "air"})
				minetest.sound_play(
					"default_steam",
					{
						pos = pos,
						gain = 1.0,
						max_hear_distance = 10
					}
				)
			end
		})
	end


	--
	-- Environmental sounds
	--

	if (minetest.settings:get_bool("default_abm_sound_1") ~= false) then
		minetest.register_abm({
			label = "Lava sound",

			-- Nodes that emit the sound
			nodenames = {
				"default:02_05", -- Lava source
				"default:02_06"  -- Flowing lava
			},

			-- Nodes required to be nearby.
			neighbors = {},

			interval = 15.0, -- Seconds
			chance = 3, -- 1: 100%; 2: 50%; 3: 33%; etc.
			catch_up = false, -- See lua_api.txt at line 4085 (v. 0.4.17.1)

			action = function(pos, node, active_object_count, active_object_count_wider)
				minetest.sound_play(
					"default_lava",
					{
						pos = pos,
						gain = 0.5,
						max_hear_distance = 10
					}
				)
			end
		})
	end
end

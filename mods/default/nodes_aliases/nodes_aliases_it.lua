--[[

	Copyright © 2018-2019 Hamlet <hamlatmesehub@riseup.net>

	Licensed under the EUPL, Version 1.2 or – as soon they will be
	approved by the European Commission – subsequent versions of the
	EUPL (the "Licence");
	You may not use this work except in compliance with the Licence.
	You may obtain a copy of the Licence at:

	https://joinup.ec.europa.eu/software/page/eupl
	https://eur-lex.europa.eu/legal-content/EN/TXT/?uri=CELEX:32017D0863

	Unless required by applicable law or agreed to in writing,
	software distributed under the Licence is distributed on an
	"AS IS" basis,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
	implied.
	See the Licence for the specific language governing permissions
	and limitations under the Licence.

--]]


-- Solids

minetest.register_alias("pietra", "default:01_01")

minetest.register_alias("terra", "default:01_02")

minetest.register_alias("terra_con_erba", "default:01_03")

minetest.register_alias("sabbia", "default:01_04")

minetest.register_alias("ghiaia", "default:01_05")

minetest.register_alias("pietra_del_deserto", "default:01_06")

minetest.register_alias("sabbia_del_deserto", "default:01_07")

minetest.register_alias("terra_con_neve", "default:01_08")

minetest.register_alias("blocco_di_neve", "default:01_09")

minetest.register_alias("neve", "default:01_10")

minetest.register_alias("ghiaccio", "default:01_11")


-- Liquids
minetest.register_alias("acqua", "default:02_01")

minetest.register_alias("acqua_di_fiume", "default:02_03")

minetest.register_alias("lava", "default:02_05")

--[[

	Copyright © 2018 Hamlet <hamlatmesehub@riseup.net>

	Licensed under the EUPL, Version 1.2 or – as soon they will be
	approved by the European Commission – subsequent versions of the
	EUPL (the "Licence");
	You may not use this work except in compliance with the Licence.
	You may obtain a copy of the Licence at:

	https://joinup.ec.europa.eu/software/page/eupl
	https://eur-lex.europa.eu/legal-content/EN/TXT/?uri=CELEX:32017D0863

	Unless required by applicable law or agreed to in writing,
	software distributed under the Licence is distributed on an
	"AS IS" basis,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
	implied.
	See the Licence for the specific language governing permissions
	and limitations under the Licence.

--]]


-- Base terrain

minetest.register_alias("mapgen_stone", "default:01_01")
minetest.register_alias("mapgen_water_source", "default:02_01")
minetest.register_alias("mapgen_river_water_source", "default:02_03")


-- Caves

minetest.register_alias("mapgen_lava_source", "default:02_05")


-- Dungeons

--minetest.register_alias("mapgen_cobble", )
--minetest.register_alias("mapgen_stair_cobble", )
--minetest.register_alias("mapgen_mossycobble", )

--minetest.register_alias("mapgen_desert_stone", )
--minetest.register_alias("mapgen_stair_desert_stone", )

--minetest.register_alias("mapgen_sandstone", )
--minetest.register_alias("mapgen_sandstonebrick", )
--minetest.register_alias("mapgen_stair_sandstone_block", )

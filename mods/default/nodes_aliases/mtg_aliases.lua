--[[

	Copyright © 2018 Hamlet <hamlatmesehub@riseup.net>

	Licensed under the EUPL, Version 1.2 or – as soon they will be
	approved by the European Commission – subsequent versions of the
	EUPL (the "Licence");
	You may not use this work except in compliance with the Licence.
	You may obtain a copy of the Licence at:

	https://joinup.ec.europa.eu/software/page/eupl
	https://eur-lex.europa.eu/legal-content/EN/TXT/?uri=CELEX:32017D0863

	Unless required by applicable law or agreed to in writing,
	software distributed under the Licence is distributed on an
	"AS IS" basis,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
	implied.
	See the Licence for the specific language governing permissions
	and limitations under the Licence.

--]]


minetest.register_alias("default:stone", "default:01_01")
minetest.register_alias("default:dirt", "default:01_02")
minetest.register_alias("default:dirt_with_grass", "default:01_03")
minetest.register_alias("default:sand", "default:01_04")
minetest.register_alias("default:gravel", "default:01_05")
minetest.register_alias("default:desert_stone", "default:01_06")
minetest.register_alias("default:desert_sand", "default:01_07")
minetest.register_alias("default:dirt_with_snow", "default:01_08")
minetest.register_alias("default:snowblock", "default:01_09")
minetest.register_alias("default:snow", "default:01_10")
minetest.register_alias("default:ice", "default:01_11")
minetest.register_alias("default:dirt_with_dry_grass", "default:01_12")
minetest.register_alias("default:dirt_with_rainforest_litter", "default:01_13")

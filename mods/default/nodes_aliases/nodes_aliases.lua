--[[

	Copyright © 2018-2019 Hamlet <hamlatmesehub@riseup.net>

	Licensed under the EUPL, Version 1.2 or – as soon they will be
	approved by the European Commission – subsequent versions of the
	EUPL (the "Licence");
	You may not use this work except in compliance with the Licence.
	You may obtain a copy of the Licence at:

	https://joinup.ec.europa.eu/software/page/eupl
	https://eur-lex.europa.eu/legal-content/EN/TXT/?uri=CELEX:32017D0863

	Unless required by applicable law or agreed to in writing,
	software distributed under the Licence is distributed on an
	"AS IS" basis,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
	implied.
	See the Licence for the specific language governing permissions
	and limitations under the Licence.

--]]


-- Detect Minetest's language

local l10n = minetest.settings:get("language")

-- Load the localized nodes' aliases

if (l10n == "be") then
	print("Locale: " .. l10n .. " not yet supported for aliases.")
	dofile(minetest.get_modpath("default") .. "/nodes_aliases/nodes_aliases/nodes_aliases_uk.lua")

elseif (l10n == "ca") then
	print("Locale: " .. l10n .. " not yet supported for aliases.")
	dofile(minetest.get_modpath("default") .. "/nodes_aliases/nodes_aliases_uk.lua")

elseif (l10n == "cs") then
	print("Locale: " .. l10n .. " not yet supported for aliases.")
	dofile(minetest.get_modpath("default") .. "/nodes_aliases/nodes_aliases_uk.lua")

elseif (l10n == "da") then
	print("Locale: " .. l10n .. " not yet supported for aliases.")
	dofile(minetest.get_modpath("default") .. "/nodes_aliases/nodes_aliases_uk.lua")

elseif (l10n == "de") then
	print("Locale: " .. l10n .. " not yet supported for aliases.")
	dofile(minetest.get_modpath("default") .. "/nodes_aliases/nodes_aliases_uk.lua")

elseif (l10n == "dv") then
	print("Locale: " .. l10n .. " not yet supported for aliases.")
	dofile(minetest.get_modpath("default") .. "/nodes_aliases/nodes_aliases_uk.lua")

elseif (l10n == "eo") then
	print("Locale: " .. l10n .. " not yet supported for aliases.")
	dofile(minetest.get_modpath("default") .. "/nodes_aliases/nodes_aliases_uk.lua")

elseif (l10n == "es") then
	print("Locale: " .. l10n .. " not yet supported for aliases.")
	dofile(minetest.get_modpath("default") .. "/nodes_aliases/nodes_aliases_uk.lua")

elseif (l10n == "et") then
	print("Locale: " .. l10n .. " not yet supported for aliases.")
	dofile(minetest.get_modpath("default") .. "/nodes_aliases/nodes_aliases_uk.lua")

elseif (l10n == "fr") then
	print("Locale: " .. l10n .. " not yet supported for aliases.")
	dofile(minetest.get_modpath("default") .. "/nodes_aliases/nodes_aliases_uk.lua")
	
elseif (l10n == "he") then
	print("Locale: " .. l10n .. " not yet supported for aliases.")
	dofile(minetest.get_modpath("default") .. "/nodes_aliases/nodes_aliases_uk.lua")

elseif (l10n == "hu") then
	print("Locale: " .. l10n .. " not yet supported for aliases.")
	dofile(minetest.get_modpath("default") .. "/nodes_aliases/nodes_aliases_uk.lua")

elseif (l10n == "id") then
	print("Locale: " .. l10n .. " not yet supported for aliases.")
	dofile(minetest.get_modpath("default") .. "/nodes_aliases/nodes_aliases_uk.lua")

elseif (l10n == "it") then
	--print("Locale: " .. l10n .. " supportato per gli alias.")
	dofile(minetest.get_modpath("default") .. "/nodes_aliases/nodes_aliases_it.lua")

elseif (l10n == "ja") then
	print("Locale: " .. l10n .. " not yet supported for aliases.")
	dofile(minetest.get_modpath("default") .. "/nodes_aliases/nodes_aliases_uk.lua")

elseif (l10n == "jbo") then
	print("Locale: " .. l10n .. " not yet supported for aliases.")
	dofile(minetest.get_modpath("default") .. "/nodes_aliases/nodes_aliases_uk.lua")

elseif (l10n == "ko") then
	print("Locale: " .. l10n .. " not yet supported for aliases.")
	dofile(minetest.get_modpath("default") .. "/nodes_aliases/nodes_aliases_uk.lua")

elseif (l10n == "ky") then
	print("Locale: " .. l10n .. " not yet supported for aliases.")
	dofile(minetest.get_modpath("default") .. "/nodes_aliases/nodes_aliases_uk.lua")

elseif (l10n == "lt") then
	print("Locale: " .. l10n .. " not yet supported for aliases.")
	dofile(minetest.get_modpath("default") .. "/nodes_aliases/nodes_aliases_uk.lua")

elseif (l10n == "ms") then
	print("Locale: " .. l10n .. " not yet supported for aliases.")
	dofile(minetest.get_modpath("default") .. "/nodes_aliases/nodes_aliases_uk.lua")

elseif (l10n == "nb") then
	print("Locale: " .. l10n .. " not yet supported for aliases.")
	dofile(minetest.get_modpath("default") .. "/nodes_aliases/nodes_aliases_uk.lua")

elseif (l10n == "nl") then
	print("Locale: " .. l10n .. " not yet supported for aliases.")
	dofile(minetest.get_modpath("default") .. "/nodes_aliases/nodes_aliases_uk.lua")

elseif (l10n == "pl") then
	print("Locale: " .. l10n .. " not yet supported for aliases.")
	dofile(minetest.get_modpath("default") .. "/nodes_aliases/nodes_aliases_uk.lua")

elseif (l10n == "pt") then
	print("Locale: " .. l10n .. " not yet supported for aliases.")
	dofile(minetest.get_modpath("default") .. "/nodes_aliases/nodes_aliases_uk.lua")

elseif (l10n == "pt_BR") then
	print("Locale: " .. l10n .. " not yet supported for aliases.")
	dofile(minetest.get_modpath("default") .. "/nodes_aliases/nodes_aliases_uk.lua")

elseif (l10n == "ro") then
	print("Locale: " .. l10n .. " not yet supported for aliases.")
	dofile(minetest.get_modpath("default") .. "/nodes_aliases/nodes_aliases_uk.lua")

elseif (l10n == "ru") then
	print("Locale: " .. l10n .. " not yet supported for aliases.")
	dofile(minetest.get_modpath("default") .. "/nodes_aliases/nodes_aliases_uk.lua")

elseif (l10n == "sl") then
	print("Locale: " .. l10n .. " not yet supported for aliases.")
	dofile(minetest.get_modpath("default") .. "/nodes_aliases/nodes_aliases_uk.lua")

elseif (l10n == "sr_Cyrl") then
	print("Locale: " .. l10n .. " not yet supported for aliases.")
	dofile(minetest.get_modpath("default") .. "/nodes_aliases/nodes_aliases_uk.lua")

elseif (l10n == "sv") then
	print("Locale: " .. l10n .. " not yet supported for aliases.")
	dofile(minetest.get_modpath("default") .. "/nodes_aliases/nodes_aliases_uk.lua")

elseif (l10n == "sw") then
	print("Locale: " .. l10n .. " not yet supported for aliases.")
	dofile(minetest.get_modpath("default") .. "/nodes_aliases/nodes_aliases_uk.lua")

elseif (l10n == "tr") then
	print("Locale: " .. l10n .. " not yet supported for aliases.")
	dofile(minetest.get_modpath("default") .. "/nodes_aliases/nodes_aliases_uk.lua")

elseif (l10n == "uk") then
	print("Locale: " .. l10n .. " is supported for aliases.")
	dofile(minetest.get_modpath("default") .. "/nodes_aliases/nodes_aliases_uk.lua")

elseif (l10n == "zh_CN") then
	print("Locale: " .. l10n .. " not yet supported for aliases.")
	dofile(minetest.get_modpath("default") .. "/nodes_aliases/nodes_aliases_uk.lua")

elseif (l10n == "zh_TW") then
	print("Locale: " .. l10n .. " not yet supported for aliases.")
	dofile(minetest.get_modpath("default") .. "/nodes_aliases/nodes_aliases_uk.lua")

else
	print("Unrecognized locale, loading english aliases.")
	dofile(minetest.get_modpath("default") .. "/nodes_aliases/nodes_aliases_uk.lua")
	
end

--[[

	Copyright © 2018 Hamlet <hamlatmesehub@riseup.net>

	Licensed under the EUPL, Version 1.2 or – as soon they will be
	approved by the European Commission – subsequent versions of the
	EUPL (the "Licence");
	You may not use this work except in compliance with the Licence.
	You may obtain a copy of the Licence at:

	https://joinup.ec.europa.eu/software/page/eupl
	https://eur-lex.europa.eu/legal-content/EN/TXT/?uri=CELEX:32017D0863

	Unless required by applicable law or agreed to in writing,
	software distributed under the Licence is distributed on an
	"AS IS" basis,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
	implied.
	See the Licence for the specific language governing permissions
	and limitations under the Licence.

--]]


-- Used to check whether if a node's name and light level
-- match the desired conditions.
mevo.NodeAndLightCheck = function(position, node_name, light_level)
	local detected_name = minetest.get_node(position).name
	local detected_light = minetest.get_node_light(position, nil)

	if (detected_name == node_name)
	and (detected_light >= light_level)
	then
		return true

	else
		return false

	end
end


-- Used to check whether if it's daytime or nigttime.
mevo.DayOrNightCheck = function()
	local current_time = (minetest.get_timeofday() * 24000)

	if (current_time >= 4700) -- Dawn
	and (current_time <= 19250) -- Dusk
	then
		return true

	else
		return false

	end
end

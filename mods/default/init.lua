--[[

	Copyright © 2018-2019 Hamlet <hamlatmesehub@riseup.net>

	Licensed under the EUPL, Version 1.2 or – as soon they will be
	approved by the European Commission – subsequent versions of the
	EUPL (the "Licence");
	You may not use this work except in compliance with the Licence.
	You may obtain a copy of the Licence at:

	https://joinup.ec.europa.eu/software/page/eupl
	https://eur-lex.europa.eu/legal-content/EN/TXT/?uri=CELEX:32017D0863

	Unless required by applicable law or agreed to in writing,
	software distributed under the Licence is distributed on an
	"AS IS" basis,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
	implied.
	See the Licence for the specific language governing permissions
	and limitations under the Licence.

--]]

mevo = {}

dofile(minetest.get_modpath("default") .. "/functions_checks.lua")
dofile(minetest.get_modpath("default") .. "/abms.lua")
dofile(minetest.get_modpath("default") .. "/nodes_solid.lua")
dofile(minetest.get_modpath("default") .. "/nodes_liquid.lua")
dofile(minetest.get_modpath("default") .. "/nodes_aliases/nodes_aliases.lua")
dofile(minetest.get_modpath("default") .. "/nodes_aliases/mapgen_aliases.lua")
dofile(minetest.get_modpath("default") .. "/biomes.lua")


--
-- Minetest Game aliases
--

-- Used to convert MeVo's nodes' names to MTG's.
if (minetest.settings:get_bool("default_mevo_to_mtg_nodenames") == true) then
	dofile(minetest.get_modpath("default") .. "/nodes_aliases/mtg_aliases.lua")
end

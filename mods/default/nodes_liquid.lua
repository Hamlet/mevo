--[[

	Copyright © 2018-2019 Hamlet <hamlatmesehub@riseup.net>

	Licensed under the EUPL, Version 1.2 or – as soon they will be
	approved by the European Commission – subsequent versions of the
	EUPL (the "Licence");
	You may not use this work except in compliance with the Licence.
	You may obtain a copy of the Licence at:

	https://joinup.ec.europa.eu/software/page/eupl
	https://eur-lex.europa.eu/legal-content/EN/TXT/?uri=CELEX:32017D0863

	Unless required by applicable law or agreed to in writing,
	software distributed under the Licence is distributed on an
	"AS IS" basis,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
	implied.
	See the Licence for the specific language governing permissions
	and limitations under the Licence.

--]]


-- Used for localization

local S = minetest.get_translator("nodes_liquid")

-- Used in "mapgen_water_source"
minetest.register_node("default:02_01", {
	description = S("akvo"),
	groups = {falling_node = 1, water = 1},
	drawtype = "liquid",
	tiles = {
		{
			name = "default_02_01_anim.png",
			animation = {
				type = "vertical_frames",
				aspect_w = 16,
				aspect_h = 16,
				length = 3.0,
			},
		},
	},
	special_tiles = {
		{
			name = "default_02_01_anim.png",
			animation = {
				type = "vertical_frames",
				aspect_w = 16,
				aspect_h = 16,
				length = 3.0,
			},
			backface_culling = false,
		},
	},
	alpha = 191,
	post_effect_color = {a=191, r=20, g=20, b=116},
	paramtype = "light",
	is_ground_content = false,
	walkable = false,
	pointable = false,
	buildable_to = true,
	liquidtype = "source",
	liquid_alternative_flowing = "default:02_02",
	drowning = 1,
	liquid_viscosity = 2,
	liquid_renewable = true
})

-- Flowing water
minetest.register_node("default:02_02", {
	description = S("akvo kiu fluas"),
	groups = {water = 1},
	drawtype = "flowingliquid",
	tiles = {"default_02_01.png"},
	special_tiles = {
		{
			name = "default_02_01_anim.png",
			animation = {
				type = "vertical_frames",
				aspect_w = 16,
				aspect_h = 16,
				length = 3.0,
			},
		},
		{
			name = "default_02_01_anim.png",
			animation = {
				type = "vertical_frames",
				aspect_w = 16,
				aspect_h = 16,
				length = 3.0,
			},
		},
	},
	alpha = 191,
	post_effect_color = {a=191, r=20, g=20, b=116},
	paramtype = "light",
	paramtype2 = "flowingliquid",
	is_ground_content = false,
	walkable = false,
	pointable = false,
	buildable_to = true,
	liquidtype = "flowing",
	liquid_alternative_flowing = "default:02_02",
	liquid_alternative_source = "default:02_01",
	liquid_range = 4,
	drowning = 1,
	liquid_viscosity = 2,
})

-- Used in "mapgen_river_water_source"
minetest.register_node("default:02_03", {
	description = S("akvo de la rivero"),
	groups = {falling_node = 1, water = 1},
	drawtype = "liquid",
	tiles = {
		{
			name = "default_02_03_anim.png",
			animation = {
				type = "vertical_frames",
				aspect_w = 16,
				aspect_h = 16,
				length = 3.0,
			},
		},
	},
	special_tiles = {
		{
			name = "default_02_03_anim.png",
			animation = {
				type = "vertical_frames",
				aspect_w = 16,
				aspect_h = 16,
				length = 3.0,
			},
			backface_culling = false,
		},
	},
	alpha = 191,
	post_effect_color = {a=191, r=0, g=131, b=242},
	paramtype = "light",
	is_ground_content = false,
	walkable = false,
	pointable = false,
	buildable_to = true,
	liquidtype = "source",
	liquid_alternative_flowing = "default:02_04",
	drowning = 1,
	liquid_viscosity = 1,
	liquid_renewable = true
})

-- Flowing river water
minetest.register_node("default:02_04", {
	description = S("akvo de la rivero kiu fluas"),
	groups = {water = 1},
	drawtype = "flowingliquid",
	tiles = {"default_02_03.png"},
	special_tiles = {
		{
			name = "default_02_03_anim.png",
			animation = {
				type = "vertical_frames",
				aspect_w = 16,
				aspect_h = 16,
				length = 3.0,
			},
		},
		{
			name = "default_02_03_anim.png",
			animation = {
				type = "vertical_frames",
				aspect_w = 16,
				aspect_h = 16,
				length = 3.0,
			},
		},
	},
	alpha = 191,
	post_effect_color = {a=191, r=0, g=131, b=242},
	paramtype = "light",
	paramtype2 = "flowingliquid",
	is_ground_content = false,
	walkable = false,
	pointable = false,
	buildable_to = true,
	liquidtype = "flowing",
	liquid_alternative_flowing = "default:02_04",
	liquid_alternative_source = "default:02_03",
	liquid_range = 4,
	drowning = 1,
	liquid_viscosity = 1,
})

-- Used in "mapgen_lava_source"
minetest.register_node("default:02_05", {
	description = S("lafo"),
	groups = {falling_node = 1, lava = 1},
	drawtype = "liquid",
	tiles = {
		{
			name = "default_02_05_anim.png",
			animation = {
				type = "vertical_frames",
				aspect_w = 16,
				aspect_h = 16,
				length = 3.0,
			},
		},
	},
	special_tiles = {
		{
			name = "default_02_05_anim.png",
			animation = {
				type = "vertical_frames",
				aspect_w = 16,
				aspect_h = 16,
				length = 3.0,
			},
			backface_culling = false,
		},
	},
	alpha = 219,
	post_effect_color = {a=219, r=184, g=82, b=0},
	paramtype = "light",
	is_ground_content = false,
	walkable = false,
	pointable = false,
	buildable_to = false,
	liquidtype = "source",
	liquid_alternative_flowing = "default:02_06",
	light_source = minetest.LIGHT_MAX,
	damage_per_second = 10,
	liquid_viscosity = 7,
	liquid_renewable = true
})

-- Flowing lava
minetest.register_node("default:02_06", {
	description = S("lafo kiu fluas"),
	groups = {lava = 1},
	drawtype = "flowingliquid",
	tiles = {"default_02_05.png"},
	special_tiles = {
		{
			name = "default_02_05_anim.png",
			animation = {
				type = "vertical_frames",
				aspect_w = 16,
				aspect_h = 16,
				length = 3.0,
			},
		},
		{
			name = "default_02_05_anim.png",
			animation = {
				type = "vertical_frames",
				aspect_w = 16,
				aspect_h = 16,
				length = 3.0,
			},
		},
	},
	alpha = 219,
	post_effect_color = {a=219, r=184, g=82, b=0},
	paramtype = "light",
	paramtype2 = "flowingliquid",
	is_ground_content = false,
	walkable = false,
	pointable = false,
	buildable_to = false,
	liquidtype = "flowing",
	liquid_alternative_flowing = "default:02_06",
	liquid_alternative_source = "default:02_05",
	liquid_range = 4,
	light_source = minetest.LIGHT_MAX,
	damage_per_second = 10,
	liquid_viscosity = 7,
})

-- Freezing water source
minetest.register_node("default:02_07", {
	description = S("akvo kiu frostas"),
	groups = {falling_node = 1, water = 1},
	drawtype = "liquid",
	tiles = {
		{
			name = "default_02_03_anim.png",
			animation = {
				type = "vertical_frames",
				aspect_w = 16,
				aspect_h = 16,
				length = 3.0,
			},
		},
	},
	special_tiles = {
		{
			name = "default_02_03_anim.png",
			animation = {
				type = "vertical_frames",
				aspect_w = 16,
				aspect_h = 16,
				length = 3.0,
			},
			backface_culling = false,
		},
	},
	alpha = 191,
	post_effect_color = {a=191, r=0, g=131, b=242},
	paramtype = "light",
	is_ground_content = false,
	walkable = false,
	pointable = false,
	buildable_to = true,
	liquidtype = "source",
	liquid_alternative_flowing = "default:02_08",
	damage_per_second = 2,
	drowning = 1,
	liquid_viscosity = 1,
	liquid_renewable = true
})

-- Flowing freezing water
minetest.register_node("default:02_08", {
	description = S("akvo kiu frostas kiu fluas"),
	groups = {water = 1},
	drawtype = "flowingliquid",
	tiles = {"default_02_03.png"},
	special_tiles = {
		{
			name = "default_02_03_anim.png",
			animation = {
				type = "vertical_frames",
				aspect_w = 16,
				aspect_h = 16,
				length = 3.0,
			},
		},
		{
			name = "default_02_03_anim.png",
			animation = {
				type = "vertical_frames",
				aspect_w = 16,
				aspect_h = 16,
				length = 3.0,
			},
		},
	},
	alpha = 191,
	post_effect_color = {a=191, r=0, g=131, b=242},
	paramtype = "light",
	paramtype2 = "flowingliquid",
	is_ground_content = false,
	walkable = false,
	pointable = false,
	buildable_to = true,
	liquidtype = "flowing",
	liquid_alternative_flowing = "default:02_08",
	liquid_alternative_source = "default:02_07",
	damage_per_second = 2,
	liquid_range = 4,
	drowning = 1,
	liquid_viscosity = 1,
})

--[[

	Copyright © 2018 Hamlet <hamlatmesehub@riseup.net>

	Licensed under the EUPL, Version 1.2 or – as soon they will be
	approved by the European Commission – subsequent versions of the
	EUPL (the "Licence");
	You may not use this work except in compliance with the Licence.
	You may obtain a copy of the Licence at:

	https://joinup.ec.europa.eu/software/page/eupl
	https://eur-lex.europa.eu/legal-content/EN/TXT/?uri=CELEX:32017D0863

	Unless required by applicable law or agreed to in writing,
	software distributed under the Licence is distributed on an
	"AS IS" basis,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
	implied.
	See the Licence for the specific language governing permissions
	and limitations under the Licence.

--]]


--
-- Koepper's biomes
--

-- TROPICAL HUMID CLIMATES
-- Rainforest
minetest.register_biome({
	name = "Af",
	node_dust = "",
	node_top = "default:01_13",
	-- dirt
	depth_top = 1,
	node_filler = "default:01_02",
	-- dirt
	depth_filler = math.random(5, 20),
	node_stone = "",
	node_water_top = "",
	depth_water_top = 0,
	node_water = "default:02_01",
	node_river_water = "default:02_03",
	node_river_bed = "default:01_05",
	-- gravel
	depth_river_bed = math.random(1, 3),
	y_min = 1,
	y_max = 120,
	heat_point = 63, -- 25°C/77°F
	humidity_point = 72 -- %
})

-- Monsonic
minetest.register_biome({
	name = "Am",
	node_dust = "",
	node_top = "default:01_13",
	-- dirt
	depth_top = 1,
	node_filler = "default:01_02",
	-- dirt
	depth_filler = math.random(5, 20),
	node_stone = "",
	node_water_top = "",
	depth_water_top = 0,
	node_water = "default:02_01",
	node_river_water = "default:02_03",
	node_river_bed = "default:01_05",
	-- gravel
	depth_river_bed = math.random(1, 3),
	y_min = 1,
	y_max = 120,
	heat_point = 63, -- 25°C/77°F
	humidity_point = 73 -- %
})

-- Savannah
minetest.register_biome({
	name = "Aw",
	node_dust = "",
	node_top = "default:01_12",
	-- dirt with dry grass
	depth_top = 1,
	node_filler = "default:01_02",
	-- dirt
	depth_filler = math.random(5, 20),
	node_stone = "",
	node_water_top = "",
	depth_water_top = 0,
	node_water = "default:02_01",
	node_river_water = "default:02_03",
	node_river_bed = "default:01_05",
	-- gravel
	depth_river_bed = math.random(1, 3),
	y_min = 1,
	y_max = 120,
	heat_point = 63, -- 25°C/77°F
	humidity_point = 57 -- %
})


-- ARID CLIMATES
-- Steppa
minetest.register_biome({
	name = "BS",
	node_dust = "",
	node_top = "default:01_12",
	-- dirt with dry grass
	depth_top = 1,
	node_filler = "default:01_02",
	-- dirt
	depth_filler = math.random(1, 5),
	node_stone = "",
	node_water_top = "",
	depth_water_top = 0,
	node_water = "default:02_01",
	node_river_water = "default:02_03",
	node_river_bed = "default:01_05",
	-- gravel
	depth_river_bed = math.random(1, 3),
	y_min = 1,
	y_max = 120,
	heat_point = 55, -- 10°C/50°F
	humidity_point = 52 -- %
})

-- Desertic cold
minetest.register_biome({
	name = "BWk",
	node_dust = "default:01_10",
	-- snow
	node_top = "default:01_04",
	-- sand
	depth_top = 1,
	node_filler = "default:01_04",
	-- sand
	depth_filler = math.random(1, 5),
	node_stone = "",
	node_water_top = "",
	depth_water_top = 0,
	node_water = "default:02_01",
	node_river_water = "air",
	node_river_bed = "default:01_04",
	depth_river_bed = math.random(1, 3),
	y_min = 1,
	y_max = 120,
	heat_point = 50, -- 0°C/32°F
	humidity_point = 66 -- %
})

-- Desertic warm
minetest.register_biome({
	name = "BWh",
	node_dust = "",
	node_top = "default:01_07",
	-- desert sand
	depth_top = 1,
	node_filler = "default:01_07",
	-- desert sand
	depth_filler = math.random(5, 20),
	node_stone = "default:01_06",
	-- desert stone
	node_water_top = "",
	depth_water_top = 0,
	node_water = "default:02_01",
	node_river_water = "air",
	node_river_bed = "default:01_07",
	depth_river_bed = math.random(1, 3),
	y_min = 1,
	y_max = 120,
	heat_point = 65, -- 30°C/86°F
	humidity_point = 48 -- %
})


-- WARM HUMID TEMPERATE CLIMATES
-- Warm with dry winters
minetest.register_biome({
	name = "Cw",
	node_dust = "",
	node_top = "default:01_03",
	-- dirt with grass
	depth_top = 1,
	node_filler = "default:01_02",
	-- dirt
	depth_filler = math.random(5, 20),
	node_stone = "",
	node_water_top = "",
	depth_water_top = 0,
	node_water = "default:02_01",
	node_river_water = "default:02_03",
	node_river_bed = "default:01_05",
	depth_river_bed = math.random(1, 3),
	y_min = 1,
	y_max = 120,
	heat_point = 64, -- 28°C/82.4°F
	humidity_point = 70 -- %
})

-- Warm with dry summers
minetest.register_biome({
	name = "Cs",
	node_dust = "",
	node_top = "default:01_03",
	-- dirt with grass
	depth_top = 1,
	node_filler = "default:01_02",
	-- dirt
	depth_filler = math.random(5, 20),
	node_stone = "",
	node_water_top = "",
	depth_water_top = 0,
	node_water = "default:02_01",
	node_river_water = "default:02_03",
	node_river_bed = "default:01_05",
	depth_river_bed = math.random(1, 3),
	y_min = 1,
	y_max = 120,
	heat_point = 63, -- 25°C/77°F
	humidity_point = 60 -- %
})

-- Temperate humid
minetest.register_biome({
	name = "Cf",
	node_dust = "",
	node_top = "default:01_03",
	-- dirt with grass
	depth_top = 1,
	node_filler = "default:01_02",
	-- dirt
	depth_filler = math.random(5, 20),
	node_stone = "",
	node_water_top = "",
	depth_water_top = 0,
	node_water = "default:02_01",
	node_river_water = "default:02_03",
	node_river_bed = "default:01_05",
	depth_river_bed = math.random(1, 3),
	y_min = 1,
	y_max = 120,
	heat_point = 64, -- 28°C/82.4°F
	humidity_point = 72 -- %
})


-- BOREAL CLIMATES
-- Cold with dry winters
minetest.register_biome({
	name = "Dw",
	node_dust = "",
	node_top = "default:01_08",
	-- dirt with snow
	depth_top = 1,
	node_filler = "default:01_02",
	-- dirt
	depth_filler = math.random(5, 20),
	node_stone = "",
	node_water_top = "default:01_11",
	-- ice block
	depth_water_top = math.random(1, 10),
	node_water = "default:02_07",
	node_river_water = "default:02_07",
	node_river_bed = "default:01_05",
	-- gravel
	depth_river_bed = math.random(1, 3),
	y_min = 1,
	y_max = 120,
	heat_point = 38, -- -24°C/-11.2°F
	humidity_point = 70 -- %
})

-- Cold with humid winters
minetest.register_biome({
	name = "Df",
	node_dust = "default:01_09",
	-- snow block
	node_top = "default:01_08",
	-- dirt with snow
	depth_top = 1,
	node_filler = "default:01_02",
	-- dirt
	depth_filler = math.random(5, 20),
	node_stone = "",
	node_water_top = "default:01_11",
	-- ice block
	depth_water_top = math.random(1, 10),
	node_water = "default:02_07",
	node_river_water = "default:02_07",
	node_river_bed = "default:01_05",
	-- gravel
	depth_river_bed = math.random(1, 3),
	y_min = 1,
	y_max = 120,
	heat_point = 43, -- -13°C/8.6°F
	humidity_point = 73 -- %
})


-- SNOWY CLIMATES
-- Tundra
minetest.register_biome({
	name = "ET",
	node_dust = "default:01_10",
	-- snow
	node_top = "default:01_08",
	-- dirt with snow
	depth_top = 1,
	node_filler = "default:01_02",
	-- dirt
	depth_filler = math.random(5, 20),
	node_stone = "",
	node_water_top = "default:01_11",
	-- ice block
	depth_water_top = math.random(1, 10),
	node_water = "default:02_07",
	node_river_water = "default:02_07",
	node_river_bed = "default:01_05",
	-- gravel
	depth_river_bed = math.random(1, 3),
	y_min = 1,
	y_max = 120,
	heat_point = 36, -- -28°C/-18.4°F
	humidity_point = 70 -- %
})

-- Mountain tundra
minetest.register_biome({
	name = "ETH",
	node_dust = "default:01_09",
	-- snow block
	node_top = "default:01_08",
	-- dirt with snow
	depth_top = 1,
	node_filler = "default:01_02",
	-- dirt
	depth_filler = math.random(5, 20),
	node_stone = "",
	node_water_top = "",
	depth_water_top = "",
	node_water = "",
	node_river_water = "",
	node_river_bed = "",
	depth_river_bed = 0,
	y_min = 120,
	y_max = 240,
	heat_point = 57, -- 13°C/55.4°F
	humidity_point = 70 -- %
})

-- Permafrost
minetest.register_biome({
	name = "EF",
	node_dust = "default:01_10",
	-- snow
	node_top = "default:01_08",
	-- dirt with snow
	depth_top = 1,
	node_filler = "default:01_02",
	-- dirt
	depth_filler = math.random(5, 20),
	node_stone = "",
	node_water_top = "default:01_11",
	-- ice block
	depth_water_top = math.random(1, 10),
	node_water = "default:02_07",
	node_river_water = "default:02_07",
	node_river_bed = "default:01_05",
	-- gravel
	depth_river_bed = math.random(1, 3),
	y_min = 1,
	y_max = 120,
	heat_point = 30, -- -40°C/-40°F
	humidity_point = 60 -- %
})

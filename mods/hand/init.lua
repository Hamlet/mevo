--[[

	Copyright © 2019 Hamlet <hamlatmesehub@riseup.net>

	Licensed under the EUPL, Version 1.2 or – as soon they will be
	approved by the European Commission – subsequent versions of the
	EUPL (the "Licence");
	You may not use this work except in compliance with the Licence.
	You may obtain a copy of the Licence at:

	https://joinup.ec.europa.eu/software/page/eupl
	https://eur-lex.europa.eu/legal-content/EN/TXT/?uri=CELEX:32017D0863

	Unless required by applicable law or agreed to in writing,
	software distributed under the Licence is distributed on an
	"AS IS" basis,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
	implied.
	See the Licence for the specific language governing permissions
	and limitations under the Licence.

--]]


-- Definition and registration of the right hand

minetest.register_item(

	-- Name
	":",
	
	{
	
	type = "none",
	
	-- Item definition
	-- See lua_api.txt at line 5807 (Minetest v5.0.1)

	wield_image = "wieldhand.png",
	-- Image to be applied

	wield_scale = {x = 1.0, y = 1.0, z = 2.5},
	-- Scale

	range = minetest.settings:get("hand_range") or 2.0,
	--[[
		Hand's reach, stated in nodes
		"1" = node where the character is standing
		"2" = node next to the aforementioned node
		etc.
	--]]

	liquids_pointable = false,
	-- Whether if it can interact with liquids
	-- Default: false, else you could take lava.

	tool_capabilities = {

		-- Time required for full effect, stated in seconds
		-- See lua_api.txt at line 1578 (Minetest v5.0.1)
		full_punch_interval = 1.0,
		
		-- Amount of useful items to be dropped, integer
		-- See lua_api.txt at line 1591 (Minetest v5.0.1)
		max_drop_level = 1,
		
		-- Effectiveness on nodes groups
		-- See lua_api.txt at line 1644 (Minetest v5.0.1)
		groupcaps = {
			crumbly = {
				maxlevel = minetest.settings:get("hand_crumbly_maxlevel") or 0,
				--[[
					Node's toughness:

					0 - Indestructible
					1 - Hard
					2 - Normal
					3 - Medium
					4 - Soft
				--]]
				
				-- uses = 1,
				--[[
					Wear amount, from 0 to 65535
					Disabled = infinite uses
					See lua_api.txt at line 1352 (Minetest v5.0.1)
				--]]

				times = {
					[0] = minetest.settings:get("hand_time_crumbly_0") or 1.0,
					[1] = minetest.settings:get("hand_time_crumbly_1") or 1.0,
					[2] = minetest.settings:get("hand_time_crumbly_2") or 1.0,
					[3] = minetest.settings:get("hand_time_crumbly_3") or 1.0,
					[4] = minetest.settings:get("hand_time_crumbly_4") or 1.0
				}
				-- Seconds required to dig a node having toughness N.
				-- See lua_api.txt at line 1352 (Minetest v5.0.1)
			},
			
			soil = {
				maxlevel = minetest.settings:get("hand_soil_maxlevel") or 0,
				times = {
					[0] = minetest.settings:get("hand_time_soil_0") or 1.0,
					[1] = minetest.settings:get("hand_time_soil_1") or 1.0,
					[2] = minetest.settings:get("hand_time_soil_2") or 1.0,
					[3] = minetest.settings:get("hand_time_soil_3") or 1.0,
					[4] = minetest.settings:get("hand_time_soil_4") or 1.0
				}
			},
			
			-- Damage dealt
			-- See lua_api.txt at line 1676 (Minetest v5.0.1)
			damage_groups = {
				fleshy = minetest.settings:get("hand_fleshy_damage") or 2
				-- 1 heart = 2 hit points
			}
		}
	}
})
